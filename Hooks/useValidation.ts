import { dir } from "console";
import React, { useState, useEffect } from "react";
import {
  KeyType,
  stateDataInterface,
  validationInterface,
} from "../Hooks/types/hooks.types";

export const useValidation = (
  stateSchema: Record<KeyType, stateDataInterface>,
  validationSchema: Record<KeyType, validationInterface> = {}
) => {
  const [state, setState] = useState(stateSchema);
  const [isDirty, setIsDirty] = useState(false);
  const [disable, setDisable] = useState(true);
  const [errors, setErrors] = useState({});
  const [values, setValues] = useState({});
  const [dirty, setDirty] = useState({});
  const [signUpSection, setSignUpSection] = useState(false)
  const [authSection, setAuthSection] = useState('signup')
 


  useEffect(() => {
    const validateErrors = async () => {
      /* verify whether all vinput were typed  */
      let noDirty = await Object.keys(stateSchema).some((property) => {
        const dirtyValue: boolean = dirty[property];
        return !dirtyValue;
      });

      /* verify whether there are errors */

      let noErrors = await Object.keys(stateSchema).some((property) => {
        const errorValue: string = errors[property];
        return errorValue !== null;
      });


      await setDisable(!noDirty && !noErrors ? false : true);
    };

    validateErrors();
  }, [dirty]);



  function verifyRegExp(pattern, value) {
    const testRegexp = new RegExp(pattern);
    const testResult: boolean = testRegexp.test(value);
    return testResult;
  }

  async function validateForm(name, value) {
    /* if validation schema doesnt have anything to validate just return */
    if (!validationSchema[name]) return;
    const { required, validator } = validationSchema[name];
    const { regexp, error: errorLabel } = validator;

    let isInitialized: boolean = !value && required ? false : true;

    let error;
    if (!isInitialized) return error;

    const testRegExp = await verifyRegExp(regexp, value);

    error = !testRegExp ? errorLabel : null;

    return error;
  }

  async function checkOnBlur(e: React.FocusEvent<HTMLInputElement>) {
    const { name, value } = e.target;
    const error = await validateForm(name, value);
    setErrors((prevValues) => ({ ...prevValues, [name]: error }));
  }


  async function updateProperties(
    name: string,
    error: string,
    value: string,
    dirtyValue: boolean
  ) {
    /* Update properties individually */
    await Promise.all([
      setErrors((prevValues) => ({ ...prevValues, [name]: error })),
      setValues((prevValues) => ({ ...prevValues, [name]: value })),
      setDirty((prevValues) => ({ ...prevValues, [name]: dirtyValue })),
    ])
      .then(() => {
        return Promise.resolve();
      })
      .catch((err) => {
        console.error(err);
      });

      
  }


  async function handleData(e: React.ChangeEvent<HTMLInputElement>) {
    const { name, value } = e.target;

    let dirtyValue: boolean = value.length < 1 ? false : true;

    const error = await validateForm(name, value);

    await updateProperties(name, error, value, dirtyValue);


    await setState({
      ...state,
      [name]:{
        value, 
        error
      }
    })

    return;
  }



  async function handleLoginRequest(){

  }

  async function handleResetRequest(){

  }



  async function handleSignUpRequest(){
    await setSignUpSection(true)
  }

  async function submitData(e: React.MouseEvent<HTMLFormElement>) {
    e.preventDefault();
    if (disable === true) return;

    /* Verify the section that was typed */
    switch (authSection) { 
      case 'signup':
        handleSignUpRequest() 
        break;
      case 'login':
        handleLoginRequest()
        break; 
      default:
        handleResetRequest()
        break;
    }    
  }

  return {
    handleData,
    submitData,
    checkOnBlur,
    errors,
    values,
    dirty,
    disable,
    setValues, 
    setErrors, 
    setDirty,
    signUpSection,
    state,
    setState,
    setAuthSection
  };
};
