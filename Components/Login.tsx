import React, { useRef, useState, useEffect } from 'react'
import DragAndDrop from '../Components/DragDrop'
import login from '../styles/Login.module.css'
import { useValidation } from '../Hooks/useValidation'
import { stateDataInterface, KeyType, validationInterface } from '../Hooks/types/hooks.types'




const SecondSignUpModule = () => {
    const reducer = (state, action) => {
        switch (action.type) {
            case 'SET_DROP_DEPTH':
                return { ...state, dropDepth: action.dropDepth }
            case 'SET_IN_DROP_ZONE':
                return { ...state, inDropZone: action.inDropZone };
            case 'ADD_FILE_TO_LIST':
                return { ...state, fileList: state.fileList.concat(action.files) };
            default:
                return state;
        }
    };

    const [data, dispatch] = React.useReducer(
        reducer, { dropDepth: 0, inDropZone: false, fileList: [] }
    )
    return (
        <section className={login.signup_module}>
            <section className={login.section_stepper}>
                {/* this component can be export by another file */}

                <div className={login.div_stepper}>
                    {/* first and require step */}
                    <div className={login.div_stepper_1}>
                        <div className={login.div_utils}>
                            <div className={login.div_symbol}>Symbol</div>
                            <span className={login.span_text}>Datos principales</span>
                            <span className={login.span_status}> Completado </span>
                        </div>
                        <div className={login.div_line}>
                            <div className={login.line}></div>
                        </div>
                    </div>
                    {/* second and optional step */}
                    <div className={login.div_stepper_2}>
                        <div className={login.div_utils}>
                            <span>Symbol</span>
                            <span>Text notification</span>
                            <span className={login.span_process}> Status </span>
                        </div>
                        <div className={login.div_line}>
                            <div>
                                This process is optional, yoy can skip it and
                                completed your data then
                           </div>
                        </div>
                    </div>
                </div>
            </section>

            <form className={login.form_signup}>
                <DragAndDrop data={data} dispatch={dispatch} />
                <ol className="dropped-files">
                    {data.fileList.map(f => {
                        console.log('FIle', f)
                        return (
                            <li key={f.name}>{f.name}</li>
                        )
                    })}
                </ol>
                <div className={login.div_inputs}>
                    <input
                        className={login.input_city}
                        placeholder="ciudad"
                    />
                    <input
                        className={login.input_phone}
                        placeholder="telefono"
                    />
                </div>
                <textarea
                    name="message"
                    rows="5"
                    cols="10"
                    placeholder="Descripcion"
                />

            </form>
        </section>
    )


}


const AuthSection = ({ setSection, state, submitData, handleData, checkOnBlur, errors, styleClass, disable, buttonText }) => {

    return (


        <div>
            <section className={login.section}>
                <a className={login.anchor} onClick={() => { setSection('signup') }}>Sign up</a>
                <a className={login.anchor} onClick={() => { setSection('login') }}>Log in</a>
                <a className={login.anchor} onClick={() => { setSection('reset') }}>Reset</a>
            </section>
            <div className={login.container_center}>


                <form className={login.form} onSubmit={submitData}>
                    <section className={styleClass} >
                        <input
                            placeholder='Email'
                            name='email'
                            className={login.input}
                            onChange={handleData}
                            autoComplete="off"
                            onBlur={checkOnBlur}
                            value={state.email.value}
                        />
                        <span>{errors.email}</span>
                        <input
                            name='password'
                            placeholder='Password'
                            className={login.input}
                            onChange={handleData}
                            autoComplete="off"
                            onBlur={checkOnBlur}
                            value={state.password.value}
                        />
                        <span>{errors.password}</span>
                        <input
                            name='firstName'
                            placeholder='First Name'
                            className={login.input}
                            onChange={handleData}
                            autoComplete="off"
                            onBlur={checkOnBlur}
                            value={state.firstName.value}
                        />
                        <span>{errors.firstName}</span>
                        <input
                            name='lastName'
                            placeholder='Last Name'
                            className={login.input}
                            onChange={handleData}
                            autoComplete="off"
                            onBlur={checkOnBlur}
                            value={state.lastName.value}
                        />
                        <span>{errors.lastName}</span>
                    </section>
                    <section className={login.section_buttons}>
                        <button className={login.button} disabled={disable}>{buttonText}</button>
                    </section>
                </form>
            </div>
        </div>


    )
}






const Login: React.FC = () => {



    const [styleClass, setStyleClass] = useState(login.section_signup)
    const [buttonText, setButtonText] = useState('Sign Up')
    const [buttonStyle, setButtonStyle] = useState(login.button_signup)


    const emailRef = useRef(null)
    const passwordRef = useRef(null)
    const firstNameRef = useRef(null)
    const lastNameRef = useRef(null)




    const stateSchema: Record<KeyType, stateDataInterface> = {
        email: { value: '', error: '' },
        password: { value: '', error: '' },
        firstName: { value: '', error: '' },
        lastName: { value: '', error: '' }
    }


    const [stateTest, setStateTest] = useState<Record<KeyType, stateDataInterface>>({
        email: { value: '', error: '' },
        password: { value: '', error: '' },
        firstName: { value: '', error: '' },
        lastName: { value: '', error: '' }
    })

    const [validateSche, setValidateSche] = useState<Record<KeyType, validationInterface>>({
        email: {
            required: true,
            validator: {
                regexp: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
                error: 'Invalid email format.',
            }
        },
        password: {
            required: true,
            validator: {
                regexp: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{8,30}$/,
                error: 'Invalid password format.',
            }
        },
        firstName: {
            required: true,
            validator: {
                regexp: /^[a-zA-Z]+$/,
                error: 'Invalid Full name format.',
            }
        },
        lastName: {
            required: true,
            validator: {
                regexp: /^[a-zA-Z]+$/,
                error: 'Invalid username format.',
            }
        }
    })

    const validationSchema: Record<KeyType, validationInterface> = {
        email: {
            required: true,
            validator: {
                regexp: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
                error: 'Invalid email format.',
            }
        },
        password: {
            required: true,
            validator: {
                regexp: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{8,30}$/,
                error: 'Invalid password format.',
            }
        },
        firstName: {
            required: true,
            validator: {
                regexp: /^[a-zA-Z]+$/,
                error: 'Invalid Full name format.',
            }
        },
        lastName: {
            required: true,
            validator: {
                regexp: /^[a-zA-Z]+$/,
                error: 'Invalid username format.',
            }
        }
    }

    const { handleData, disable, submitData, checkOnBlur, errors, setDirty, setErrors, setValues, state, setState, setAuthSection, signUpSection } = useValidation(stateTest, validateSche)





    async function setSignUpSchema() {
        await Promise.all([
            setStyleClass(login.section_signup),
            setButtonStyle(login.button_signup),
            setButtonText('Sign Up'),
            setValidateSche({
                email: {
                    required: true,
                    validator: {
                        regexp: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
                        error: 'Invalid email format.',
                    }
                },
                password: {
                    required: true,
                    validator: {
                        regexp: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{8,30}$/,
                        error: 'Invalid password format.',
                    }
                },
                firstName: {
                    required: true,
                    validator: {
                        regexp: /^[a-zA-Z]+$/,
                        error: 'Invalid Full name format.',
                    }
                },
                lastName: {
                    required: true,
                    validator: {
                        regexp: /^[a-zA-Z]+$/,
                        error: 'Invalid username format.',
                    }
                }
            }),
            setStateTest({
                email: { value: '', error: '' },
                password: { value: '', error: '' },
                firstName: { value: '', error: '' },
                lastName: { value: '', error: '' }
            })
        ])

    }

    async function setLogInSchema() {
        /* Update state y validationSchema */
        await Promise.all([
            setStyleClass(login.section_login),
            setButtonStyle(login.button_login),
            setButtonText('Log In'),
            setValidateSche({
                email: {
                    required: true,
                    validator: {
                        regexp: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
                        error: 'Invalid email format.',
                    }
                },
                password: {
                    required: true,
                    validator: {
                        regexp: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{8,30}$/,
                        error: 'Invalid password format.',
                    }
                }
            }),
            setStateTest({
                email: { value: '', error: '' },
                password: { value: '', error: '' }
            })
        ])


    }


    async function setResetSchema() {
        await Promise.all([
            setStyleClass(login.section_reset),
            setButtonStyle(login.button_reset),
            setButtonText('Reset'),
            setValidateSche({
                email: {
                    required: true,
                    validator: {
                        regexp: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
                        error: 'Invalid email format.',
                    }
                }
            }),
            setStateTest({
                email: { value: '', error: '' }
            })
        ])


    }

    async function setSection(section) {

        /* Reset those properties whe user change among options */
        await Promise.all([
            setDirty({}),
            setErrors({}),
            setValues({}),
            setState(stateSchema),
            setAuthSection(section)
        ])


        /* Tasks */

        /* if the text is removed, block again the button => DONE */

        /* Show errors also in username and lastname => DONE*/

        /* update the value to the state that is injected in the hook => DONE*/

        /* if the user change between sections, clear the inputs => DONE */

        /* Change the validation structure according to the option required  => DONE*/

        /* Verify that this modification is updated in tha useValidation Hook => DONE*/



        switch (section) {
            case 'signup':
                setSignUpSchema()
                break;
            case 'login':
                setLogInSchema()
                break;
            default:
                setResetSchema()
                break;
        }
    }


    return (
        <div className={signUpSection ? login.container_form : login.container_large}>

            {signUpSection ?
                <AuthSection
                    disable={disable}
                    buttonText={buttonText}
                    styleClass={styleClass}
                    submitData={submitData}
                    handleData={handleData}
                    checkOnBlur={checkOnBlur}
                    errors={errors}
                    state={state}
                    setSection={setSection}
                />
                : <SecondSignUpModule />
            }
        </div>
    )
}

export default Login